# ics-ans-keepalived

Ansible playbook to install keepalived.

# Variables
```yaml
keepalived_email_from: '{{ inventory_hostname }}@ess.eu'
keepalived_email_notify: stephane.armanet@ess.eu
keepalived_interface: eth1
keepalived_router_id: TEST_VRRP
keepalived_smtp: 172.20.250.14
keepalived_vips:
  - test_VIP:
      advert_int: 1
      auth_password: test_hapasswd
      backup_hostname: keepalived-default-bkp
      interface: "{{ keepalived_interface }}"
      master_hostname: keepalived-default
      vip:
        - 192.168.100.100/24 dev {{ keepalived_interface }}
      virtual_router_id: 100

```

## License

BSD 2-clause
