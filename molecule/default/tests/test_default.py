import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('keepalived')


def test_keepalived(host):
    # TODO: implement at least a test
    service = host.service("keepalived")
    assert service.is_running
    assert service.is_enabled
